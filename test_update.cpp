#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <iostream>
#include <sstream>

static int callback(void *data, int argc, char **argv, char **azColName)
{
   int i;

   fprintf(stderr, "%s: ", (const char*)data); // displays the const string

   // Prints the data for each value in the table
   for(i = 0; i<argc; i++)
   {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }

   printf("\n");
   return 0;
}


int main(int argc, char * argv[])
{
  sqlite3 *db;
  char *zErrMsg = 0;
  int rc;
  char *sql;
  //string sql;
  const char* data = "Callback function called";

  // Open database
  rc = sqlite3_open("test.db", &db);

  if (rc)
  {
    fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
    return(0);
  }else
  {
    fprintf(stderr, "Opened database succesfully\n");
  }

  // /* Create SQL statement */
  //   sql = "SELECT * from COMPANY";
  /* Create merged SQL statement */


  int salary_n = 8999;
  int age_n = 19;

    //Update SQL operation
    // sql = "UPDATE COMPANY set SALARY = 25000.00 where ID=1; " \
    //       "SELECT * from COMPANY";

    //

  std::ostringstream temp;
  std::string command;

  // sql = "UPDATE COMPANY set (SALARY) = ('" + std::to_string(salary_n) + ") where ID=1;" \
  //       "SELECT * from COMPANY";

  temp.str("");
  temp << "UPDATE COMPANY SET SALARY = ("<< salary_n <<"), AGE = ("<< age_n <<") where ID=1;";
  std::cout << temp.str() << std::endl;
  command = temp.str();

  //rc = sqlite3_exec(db, command.c_str(), callback2, 0, &zErrMsg);
  rc = sqlite3_exec(db, command.c_str(), callback, (void *)data, &zErrMsg);

  // Execute SQL statement
  //rc = sqlite3_exec(db, sql, callback, (void *)data, &zErrMsg);

  if( rc != SQLITE_OK )
  {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
  } else
  {
        fprintf(stdout, "Operation done successfully\n");
  }


  sqlite3_close(db);
  return 0;


}
